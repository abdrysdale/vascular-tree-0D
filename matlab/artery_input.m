function [a_cof] = artery_input

scale_Rsys = 0.1;
scale_Csys = 1;
scale_Rpulm = 0.2;
scale_Cpulm = 1;

scale_Rsys = 0.7;
scale_Csys = .8;
scale_Rpulm = 1;
scale_Cpulm = 1;

sys.Ras = 0.003*scale_Rsys;
sys.Rat = 0.05*scale_Rsys;
sys.Rar = 0.5*scale_Rsys;
sys.Rcp = 0.52*scale_Rsys;
sys.Rvn = 0.075;
sys.Cas = 0.08*scale_Csys;
sys.Cat = 1.6*scale_Csys;
sys.Cvn = 20.5*scale_Csys;
sys.Las = 6.2e-5;
sys.Lat = 1.7e-3;

pulm.Ras = 0.002*scale_Rpulm;
pulm.Rat = 0.01*scale_Rpulm;
pulm.Rar = 0.05*scale_Rpulm;
pulm.Rcp = 0.25*scale_Rpulm;
pulm.Rvn = 0.006;
pulm.Cas = 0.18*scale_Cpulm;
pulm.Cat = 3.8*scale_Cpulm;
pulm.Cvn = 20.5*scale_Cpulm;
pulm.Las = 5.2e-5;
pulm.Lat = 1.7e-3;

rho = 1.06;  %g/cm^3


a_cof = struct('sys',sys,'pulm',pulm,'rho', rho);
