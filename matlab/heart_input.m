function [heart_coeff] = heart_input(T)

scale_EmaxLV = .2;
scale_EmaxRV = .8;
scale_Emax = 1;
ielast = 1;

%left ventricle
LV.Ks = 4e-9;
if ielast == 1
    LV.Emin = 0.1;
    LV.Emax = 2.5*scale_EmaxLV;
elseif ielast == 2
    LV.Emin = 0.08;
    LV.Emax = 2.5*scale_EmaxLV; %3;
end
LV.V0 = 10;
LV.v0 =135;
LV.m1 = 1.32;
LV.m2 = 27.4;
LV.tau1 = 0.269*T;
LV.tau2 = 0.452*T;
LV.onset = 0.0;

%left atrium
LA.Ks = 10e-9;
if ielast == 1
    LA.Emin = 0.15;
    LA.Emax = 0.25*scale_Emax;
elseif ielast == 2
    LA.Emin = 0.08;
    LA.Emax = 0.17;
end
LA.V0 = 3;
LA.v0 =27;
LA.m1 = 1.32;
LA.m2 = 13.1;
LA.tau1 = 0.11*T;
LA.tau2 = 0.18*T;
LA.onset = 0.85*T;

%right ventricle
RV.Ks = 20e-9;
if ielast == 1
    RV.Emin = 0.1;
    RV.Emax = 1.15*scale_EmaxRV;
elseif ielast == 2
    RV.Emin = 0.04;
    RV.Emax = 0.6;
end
RV.V0 = 55;
RV.v0 = 180;
RV.m1 = 1.32;
RV.m2 = 27.4;
RV.tau1 = 0.269*T;
RV.tau2 = 0.452*T;
RV.onset = 0.0;

%right atrium
RA.Ks = 10e-9;
if ielast == 1
    RA.Emin = 0.15;
    RA.Emax = 0.25*scale_Emax;
elseif ielast == 2
    RA.Emin = 0.04;
    RA.Emax = 0.15;
end
RA.V0 = 17;
RA.v0 = 40;
RA.m1 = 1.32;
RA.m2 = 13.1;
RA.tau1 = 0.11*T;
RA.tau2 = 0.18*T;
RA.onset = 0.85*T;

heart_coeff = struct('LV',LV,...
    'LA',LA,...
    'RV',RV,...
    'RA',RA );
