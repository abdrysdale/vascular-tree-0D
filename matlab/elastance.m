function [E,t]=elastance(LV,nstep,T)

t=0:T/nstep:T;
q=sin(2*pi*t/(2*T));
dt=diff(t);
v=[LV.v0 LV.v0-q(2:end).*dt];

g1=(t/(LV.tau1*T)).^LV.m1;
g2=(t/(LV.tau2*T)).^LV.m2;
G1 = g1./(1+g1);
G2 = 1./(1+g2);
k=(LV.Emax-LV.Emin)./max(G1.*G2);

E = k .* G1 .* G2  + LV.Emin;
p = E.*(v-LV.V0).*(1-LV.Ks*q);

I = find(t<=(T-LV.onset));
E = [E(I(end)+1:end) E(1:length(I))];

end



