function ftot = funcs(t, sol, a_cof, v_cof, h_cof, elast, k)

mmHg=1333;  %convert from mmHg to g/cm*s 
resist=1;
% Initialise solution vector
% ftot = zeros(22,1);

% Flows
Qav = sol(1);
Qsas = sol(2);
Qsat = sol(3);
Qtv = sol(4);
Qpv = sol(5);
Qpas = sol(6);
Qpat = sol(7);
Qmv = sol(8);

% Pressures
psas = sol(9);
psat = sol(10);
psvn = sol(11);
ppas = sol(12);
ppat = sol(13);
ppvn = sol(14);

% Volumes
Vlv = sol(15);
Vla = sol(16);
Vrv = sol(17);
Vra = sol(18);

% Valves
ksi_av = sol(19);
ksi_mv = sol(20);
ksi_pv = sol(21);
ksi_tv = sol(22);

% Blood density
rho = a_cof.rho;

% Pressures in the chambers of the heart
plv = elast.ELV(k)*(Vlv-h_cof.LV.V0);
pla = elast.ELA(k)*(Vla-h_cof.LA.V0);
prv = elast.ERV(k)*(Vrv-h_cof.RV.V0);
pra = elast.ERA(k)*(Vra-h_cof.RA.V0);

% Inductance and resistance systemic
ftot(2) = (psas-psat-a_cof.sys.Ras*Qsas)/a_cof.sys.Las;  %Qsas
ftot(3) = (psat-psvn-(a_cof.sys.Rat+a_cof.sys.Rar+a_cof.sys.Rcp)*Qsat)/a_cof.sys.Lat;  %Qsat
Qsvn = (psvn-pra)/a_cof.sys.Rvn;

% Inductance and resistance pulmonary
ftot(6) = (ppas-ppat-a_cof.pulm.Ras*Qpas)/a_cof.pulm.Las;  %Qpas
ftot(7) = (ppat-ppvn-(a_cof.pulm.Rat+a_cof.pulm.Rar+a_cof.pulm.Rcp)*Qpat)/a_cof.pulm.Lat;  %Qpat
Qpvn = (ppvn-pla)/a_cof.pulm.Rvn;

% Compliance systemic
ftot(9) = (Qav - Qsas)/a_cof.sys.Cas;  %psas
ftot(10) = (Qsas - Qsat)/a_cof.sys.Cat; %psat
ftot(11) = (Qsat - Qsvn)/a_cof.sys.Cvn; %psvn

% Compliance pulmonary
ftot(12) = (Qpv - Qpas)/a_cof.pulm.Cas;  %ppas
ftot(13) = (Qpas - Qpat)/a_cof.pulm.Cat;  %ppat
ftot(14) = (Qpat - Qpvn)/a_cof.pulm.Cvn;  %ppvn

% Volume-Flow relations systemic
ftot(15) = Qmv - Qav;   %VLV
ftot(16) = Qpvn - Qmv;   %VLA
ftot(17) = Qtv - Qpv;   %VRV
ftot(18) = Qsvn - Qtv;   %VRA

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% aortic valve (AV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% effective area of the valve
Aeff1 = (v_cof.AV.Aeffmax-v_cof.AV.Aeffmin)*ksi_av + v_cof.AV.Aeffmin;
% bernoulli resistance of the valve 
B(1) = rho/(2*Aeff1^2)*resist;
% impedance of the valve
Z(1) = rho*v_cof.AV.Leff/Aeff1;

% pressure-flow relations through valve
dpav = (plv-psas)*mmHg;
ftot(1) = (dpav-B(1)*Qav*abs(Qav) )/Z(1); %Qav

if dpav <= 0 %valve closing
    ftot(19) = ksi_av*v_cof.AV.Kvc*dpav;
elseif dpav > 0  %valve opening
    ftot(19) = (1-ksi_av)*v_cof.AV.Kvo*dpav;    %ksi_av
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% mitral valve (MV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Aeff2 = (v_cof.MV.Aeffmax-v_cof.MV.Aeffmin)*ksi_mv + v_cof.MV.Aeffmin;
B(2) = rho/(2*Aeff2^2)*resist;
Z(2) = rho*v_cof.MV.Leff/Aeff2;

% pressure-flow relations through valve
dpmv = (pla-plv)*mmHg;
ftot(8) = (dpmv-B(2)*Qmv*abs(Qmv) )/Z(2);   %Qmv

if dpmv <= 0  %valve closing
    ftot(20) = ksi_mv*v_cof.MV.Kvc*dpmv;
elseif dpmv > 0  %valve opening
    ftot(20) = (1-ksi_mv)*v_cof.MV.Kvo*dpmv;      %ksi_mv
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% pulmonary valve (PV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Aeff3 = (v_cof.PV.Aeffmax-v_cof.PV.Aeffmin)*ksi_pv + v_cof.PV.Aeffmin;
B(3) = rho/(2*Aeff3^2)*resist;
Z(3) = rho*v_cof.PV.Leff/Aeff3;

% pressure-flow relations through valve
dppv = (prv-ppas)*mmHg;
ftot(5) = (dppv-B(3)*Qpv*abs(Qpv) )/Z(3);   %Qpv

if dppv <= 0  %valve closing
    ftot(21) = ksi_pv*v_cof.PV.Kvc*dppv;
elseif dppv > 0  %valve opening
    ftot(21) = (1-ksi_pv)*v_cof.PV.Kvo*dppv;      %ksi_pv
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% tricuspic valve (TV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Aeff4 = (v_cof.TV.Aeffmax-v_cof.TV.Aeffmin)*ksi_tv + v_cof.TV.Aeffmin;
B(4) = rho/(2*Aeff4^2)*resist;
Z(4) = rho*v_cof.MV.Leff/Aeff4;

% pressure-flow relations through valve
dptv = (pra-prv)*mmHg;
ftot(4) = (dptv-B(4)*Qtv*abs(Qtv) )/Z(4);   %Qtv

if dptv <= 0  %valve closing
    ftot(22) = ksi_tv*v_cof.TV.Kvc*dptv;
elseif dptv > 0  %valve opening
    ftot(22) = (1-ksi_tv)*v_cof.TV.Kvo*dptv;      %ksi_tv
end



