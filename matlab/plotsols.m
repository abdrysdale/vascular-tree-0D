function plotsols(sol, ttot, ELA, ELV, ERV, ERA, plv, pla, prv, pra, iflag)
%% This function plots various outputs of the cardiovascular system

close all

ttot2 = ttot(1:end-1);

if iflag==1
    % figure(1)
    % plot(sol(1,:),'r')
    % ylabel('flow through aortic valve')
    % % figure(2)
    % % plot(sol(2,:),'*r')
    % % ylabel('flow in aorta')
    % figure(3)
    % plot(sol(8,:),'r')
    % ylabel('flow through mitral valve')
    %
    figure(5)
    plot(sol(15,:),'r')
    ylabel('LV volume')
    figure(6)
    plot(sol(16,:),'r')
    ylabel('LA volume')
    
    figure(7)
    H1(1)=plot(sol(19,:),'b');
    hold on
    H1(2)=plot(sol(20,:),'r');
    ylabel('valve opening (1=open)')
    legend(H1,'aortic valve','mitral valve')
    
    figure(9)
    H3(1)=plot(ELV,'r')
    hold on
    H3(2)=plot(ELA,'b')
    ylabel('elastance curves')
    legend(H3,'LV','LA')
    
    figure(10)
    H3(1)=plot(ERV,'r')
    hold on
    H3(2)=plot(ERA,'b')
    ylabel('elastance curves')
    legend(H3,'RV','RA')
    
    figure(11)
    H2(1)=plot(plv,'r');
    ylabel('LV & aortic pressure')
    hold on
    H2(2)= plot(sol(9,:),'b');
    H2(3)=plot(pla,'g-.');
    legend(H2,'LV','aorta', 'LA')
    
elseif iflag==2
    
    xplot = 4;
    yplot = 2;
    
    figure(1)
    
    subplot(xplot,yplot,1)
    H2(1)=plot(ttot2, plv,'r');
    hold on
    H2(2)= plot(ttot, sol(9,:),'b');
    H2(3)=plot(ttot2, pla,'g-.');
    Hy(1)=ylabel('pressure (mmHg)');
    legend(H2,'LV','aorta', 'LA')
    
    subplot(xplot,yplot,2)
    H2(1)=plot(ttot2,prv,'r');
    hold on
    H2(2)= plot(ttot, sol(12,:),'b');
    H2(3)=plot(ttot2,pra,'g-.');
    Hy(2)=ylabel('pressure (mmHg)');
    legend(H2,'RV','pulmonary', 'RA')
    
    subplot(xplot,yplot,3)
    H1(1)=plot(ttot, sol(19,:),'r');
    hold on
    H1(2)=plot(ttot, sol(20,:),'b');
    Hy(3)=ylabel('valve opening (1=open)');
    legend(H1,'aortic valve','mitral valve')
    
    subplot(xplot,yplot,4)
    H1(1)=plot(ttot, sol(21,:),'r');
    hold on
    H1(2)=plot(ttot, sol(22,:),'b');
    Hy(4)=ylabel('valve opening (1=open)');
    legend(H1,'pulmonary valve','tricuspid valve')
    
    subplot(xplot,yplot,5)
    H3(1)=plot(ttot,sol(1,:),'r');
    hold on
    H3(2)=plot(ttot, sol(8,:),'b');
    Hy(5)=ylabel('Q (ml/s)');
    legend(H3,'AV','MV')
    
    subplot(xplot,yplot,6)
    H3(1)=plot(ttot,sol(5,:),'r');
    hold on
    H3(2)=plot(ttot, sol(4,:),'b');
    Hy(6)=ylabel('Q (ml/s)');
    legend(H3,'PV','TV')
    
    subplot(xplot,yplot,7)
    H3(1)=plot(ttot, sol(15,:),'r');
    hold on
    H3(2)=plot(ttot, sol(16,:),'b');
    Hy(7)=ylabel('volume (ml)');
    legend(H3,'LV','LA')
    
    subplot(xplot,yplot,8)
    H3(1)=plot(ttot,sol(17,:),'r');
    hold on
    H3(2)=plot(ttot, sol(18,:),'b');
    Hy(8)=ylabel('volume (ml)');
    legend(H3,'RV','RA')
    
    % subplot(xplot,yplot,7)
    % H3(1)=plot(ttot2,ELV,'r');
    % hold on
    % H3(2)=plot(ttot2,ELA,'b');
    % Hx(1)=xlabel('time (s)');
    % Hy(7)=ylabel('elastance curves');
    % legend(H3,'LV','LA')
    %
    % subplot(xplot,yplot,8)
    % H3(1)=plot(ttot2,ERV,'r');
    % hold on
    % H3(2)=plot(ttot2,ERA,'b');
    % Hx(2)=xlabel('time (s)');
    % Hy(8)=ylabel('elastance curves');
    % legend(H3,'RV','RA')
    
    set(gcf,'Position', [153 93 1176 686])
    % set(Hx,'fontsize',12,'fontweight', 'bold')
    set(Hy,'fontsize',12,'fontweight', 'bold')
    
    % figure(2)
    % H3(1)=plot(ELV,'r');
    % hold on
    % H3(2)=plot(ELA,'b');
    % ylabel('elastance curves (mmHg/ml)');
    % legend(H3,'LV','LA')
    % set(gcf,'Position', [176 193 1153 512])
    
end

