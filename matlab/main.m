clear
close all

%% pressures are still too high and flows through the heart
%% are too high. There was a peak in the pressure

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% START INITIALISATION %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% profile on

%general input parameters
nstep = 2000;       %number of time points per cardiac cycle
T = 0.9;            %time of 1 heart beat in seconds
ncycle = 2;         %the code will run for ncycle cardiac cycles
ncycle_plot = 2;    %the last ncycle_plot cardiac cycles will be plotted
pini_sys = 80;     %initial condition for the pressure
pini_pulm = 20;     %initial condition for the pressure
iflag = 2;          %1: plot separate figures 2: plot typical heart related subplots in one figure
%2: plot one figure with 4 subplots
rk = 4;

%Define the relevant arterial coefficients
[a_cof] = artery_input;

%Define the relevant heart coefficients
[h_cof] = heart_input(T);

%Define the relevant valve coefficients
[v_cof] = valve_input;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% START CODE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate elastance curves for the different chambers of the heart
[ELV,t] = elastance(h_cof.LV,nstep,T);
[ELA,t] = elastance(h_cof.LA,nstep,T);
[ERV,t] = elastance(h_cof.RV,nstep,T);
[ERA,t] = elastance(h_cof.RA,nstep,T);

% saving heart information at the time points in structure block "elast"
elast = struct('ELV',ELV,...
    'ELA',ELA,...
    'ERV',ERV,...
    'ERA',ERA);

% saving heart information halfway the time steps in structure block "elast2"
elast2 = struct('ELV',ELV(1:end-1)+diff(ELV)/2,...
    'ELA',ELA(1:end-1)+diff(ELA)/2,...
    'ERV',ERV(1:end-1)+diff(ERV)/2,...
    'ERA',ERA(1:end-1)+diff(ERA)/2);

% Define the time step
h = T/nstep;

% Initialise the solution
sol(1,1) = 0;      %flow through aortic valve is 0
sol(2,1) = 0;      %flow through sinus is 0
sol(3,1) = 0;     %flow through aorta is 0
sol(4,1) = 0;      %flow through tricuspid is 0
sol(5,1) = 0;      %flow through pulmonary valve is 0
sol(6,1) = 0;     %flow through arteries is 0
sol(7,1) = 0;     %flow through arterioles  is 0
sol(8,1) = 0;     %flow through mitral valve is 0

sol(9,1) = pini_sys;    %initial arterial pressure
sol(10,1) = pini_sys;    %initial arterial pressure
sol(11,1) = pini_sys;    %initial arterial pressure
sol(12,1) = pini_pulm;    %initial pulmonary pressure
sol(13,1) = pini_pulm;    %initial pulmonary pressure
sol(14,1) = pini_pulm;    %initial pulmonary pressure

sol(15,1) = h_cof.LV.v0;       %end diastolic left ventricular volume
sol(16,1) = h_cof.LA.v0;      %end diastolic left atrial volume
sol(17,1) = h_cof.RV.v0;       %end diastolic right ventricular volume
sol(18,1) = h_cof.RA.v0;      %end diastolic right atrial volume

sol(19,1) = 0;    %aortic valve is initially closed
sol(20,1) = 0;    %mitral valve is closed
sol(21,1) = 0;    %pulmonary valve is closed
sol(22,1) = 0;    %tricuspid valve is closed

% Solve the system of equations using a 4th order Runge Kutta method
i=0;  % initialise

%Loop over the cardiac cycles
for icycle = 1:ncycle
    
    fprintf(1,'plotting cardiac cycle: %d\n',icycle)
    
    % loop over the time steps
    for k = 1:nstep
        
        i = i + 1;
        if rk == 2  % second order Runge Kutta
            k1 = h * feval(@funcs, t(k), sol(:,i), a_cof, v_cof, h_cof, elast, k);
            k2 = h * feval(@funcs, t(k)+h/2, sol(:,i)+k1/2, a_cof, v_cof, h_cof, elast2, k);
            sol(:,i+1) = sol(:,i) + k2;
        elseif rk == 4  % 4th order Runge Kutta
            k1 = h * feval(@funcs, t(k), sol(:,i), a_cof, v_cof, h_cof, elast, k);
            k2 = h * feval(@funcs, t(k)+h/2, sol(:,i)+k1'/2, a_cof, v_cof, h_cof, elast2, k);
            k3 = h * feval(@funcs, t(k)+h/2, sol(:,i)+k2'/2, a_cof, v_cof, h_cof, elast2, k);
            k4 = h * feval(@funcs, t(k)+h, sol(:,i)+k3', a_cof, v_cof, h_cof, elast, k+1);
            sol(:,i+1) = sol(:,i) + (k1+2*k2+2*k3+k4)'/6;
        end
        
    end
    
    % Check if solver converged
    if isnan(sum(sum(sol)))
        'NaN found in solution column'
        break
    end
    
end

% Plot only last part of the solution
if ncycle>1
    sol=sol(:,end-ncycle_plot*nstep:end);
end

elv = [];
ela = [];
erv = [];
era = [];
for i = 1:ncycle_plot
    elv = [elv elast.ELV(1:end-1)];
    ela = [ela elast.ELA(1:end-1)];
    erv = [erv elast.ERV(1:end-1)];
    era = [era elast.ERA(1:end-1)];
end

% Extract the ventricular volumes from the solution column
Vlv = sol(15,1:end-1);
Vla = sol(16,1:end-1);
Vrv = sol(17,1:end-1);
Vra = sol(18,1:end-1);

% calculate the ventricular pressures
plv = elv.*(Vlv-h_cof.LV.V0);
pla = ela.*(Vla-h_cof.LA.V0);
prv = erv.*(Vrv-h_cof.RV.V0);
pra = era.*(Vra-h_cof.RA.V0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% START VISUALISATION %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ttot_1 = (ncycle-ncycle_plot)*T;  % starting time point for plotting
ttot_2 = (ncycle)*T;              % end time point for plotting
nsteptot = ncycle_plot*nstep;     % number of time steps in plotting range
stepsizetot = (ttot_2-ttot_1)/nsteptot;    % time step size 
ttot = ttot_1:stepsizetot:ttot_2;   % total time range for plotting

% Plotting the solution
plotsols(sol, ttot, ela, elv, erv, era, plv, pla, prv, pra, iflag)















