function [valve_coeff] = valve_input
%parameter that determines valve opening/closing rate
Ktemp = 0.012; 

% aortic valve
AV.Leff = 1;   % cm
AV.Aeffmin = 1e-10;  %
AV.Aeffmax = 2;    %cm^2
AV.Kvc = Ktemp;  %0.012;
AV.Kvo = Ktemp;  %0.012;

% mitral valve
MV.Leff = 1;   %cm
MV.Aeffmin = 1e-10;
MV.Aeffmax = 7.7;  %cm^2
MV.Kvc = Ktemp*(0.03/0.012);  % 0.03;
MV.Kvo = Ktemp*(0.04/0.012);  % 0.04;

% pulmonary valve
PV.Leff = 1;   %cm
PV.Aeffmin = 1e-10;
PV.Aeffmax = 5;    %cm^2
PV.Kvc = Ktemp;  %0.012;
PV.Kvo = Ktemp;  %0.012;

% tricuspid valve
TV.Leff = 1;   %cm
TV.Aeffmin = 1e-10;
TV.Aeffmax = 8;  %cm^2
TV.Kvc = Ktemp*(0.03/0.012);  % 0.03;
TV.Kvo = Ktemp*(0.04/0.012);  % 0.04;

valve_coeff = struct('AV', AV, ...
    'MV', MV, ...
    'PV', PV, ...
    'TV', TV);
