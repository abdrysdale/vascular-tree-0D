# Vascular Tree 0D

A lumped parameter model for a full vascular tree.

- Codes written in MATALB and Fortran.
- Fortran code contains an R analysis script for graphs.
