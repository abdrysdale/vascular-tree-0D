{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  NIX_SHELL_PRESERVE_PROMPT=1;
  buildInputs = [
    pkgs.gfortran9
    pkgs.gdb
    pkgs.octave
  ];
}
